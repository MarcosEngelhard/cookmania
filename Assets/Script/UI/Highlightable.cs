﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Highlightable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool isHighlightable;
    [SerializeField]private Graphic TargetColor;
    [SerializeField] private float fadeSpeed = 3f;

    public void OnPointerEnter(PointerEventData eventData) // When the Cursor passed
    {
        isHighlightable = true;
    }

    public void OnPointerExit(PointerEventData eventData) // When the Cursor exits
    {
        isHighlightable = false;
    }

    private 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // If it's true, it's gonna appear everything, if not, is have 0 with the a
        float targetValue = isHighlightable ? 1.0f : 0.0f;
        // Get the currentColor
        Color currentColor = TargetColor.color;
        // a is the transperency. We do a Linear Intorpoletion in order to fadein/fadeout with a little "animation"
        currentColor.a = Mathf.Lerp(currentColor.a, targetValue, fadeSpeed * Time.unscaledDeltaTime);
        TargetColor.color = currentColor;
    }
}
