﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class NotePooler<T> : MonoBehaviour where T :Component //NotePoolerGeneric
{
    
    [SerializeField] private T prefab;
    

    
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    // This is a singleton

    #region Singleton
    public static NotePooler<T> Instance { get; private set; } // no one outside the script set
    [SerializeField]private Queue<T> objects = new Queue<T>();
    private void Awake() // singletoon
    {
        Instance = this;
    }

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
        PutChildOnQueue();
    }
    private void OnDisable()
    {
        objects.Clear();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public T Get()
    {
        
        //Debug.Log("Total na queue: " + objects.Count);
        //Debug.Log(objects.Peek());
        
        if (objects.Count == 0)
        {
            AddObjects(1);
            //PutChildOnQueue();
        }
        return objects.Dequeue();
    }
    
    public void ReturnToPool(T objectToReturn) // Problema: estão a ir os 2 ao mesmo tempo
    {

        //objectToReturn.transform.position = initialposition.position;
        objectToReturn.gameObject.SetActive(false);
        objects.Enqueue(objectToReturn);
        objects = new Queue<T>(objects.Distinct()); // avoid duplicated objects
        
    }
    private void AddObjects(int count)
    {
        var newObject = GameObject.Instantiate(prefab);
        newObject.transform.parent = this.transform;
        newObject.gameObject.SetActive(false);
        objects.Enqueue(newObject);
    }
    
    private void PutChildOnQueue() // Meter os filhos em queue
    {
        //Debug.Log("Colocar os filhos em queue!!");
        foreach (Transform c in transform)
        {
            if (c.gameObject.GetComponent<T>() != null)
            {
                T noteobjectcomponent = c.GetComponent<T>();

                c.gameObject.SetActive(false);

                objects.Enqueue(noteobjectcomponent);

            }
        }
    }
    
    
}
