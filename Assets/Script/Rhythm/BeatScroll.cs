﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class BeatScroll : MonoBehaviour
{
    [SerializeField]private string _path;
    private List<string> filenames = new List<string>();
    public float beatTempo;

    public bool hasStarted;
    private int i = 0;
    
    [SerializeField]private GameObject noteObjectPrefab;
    public GameObject[] NotesGeneratorArray;

    private string[] inputArray;
    private string[] timesArray;
    private float waitToRespawn;
    public static BeatScroll Instance;

    //private NotePooler NotePooler;
    private void OnEnable()
    {
        
        i = 0;
    }
    private void Awake()
    {
        //Instance path
        _path = Application.streamingAssetsPath + "/songsequence/";
        Instance = this;

        //assign notepooler instance
        //NotePooler = NotePooler.Instance;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        beatTempo = beatTempo / 60f;
        
        GetAllFiles();
    }

    // Update is called once per frame
    void Update()
    {
        //if (!hasStarted)
        //{
        //    if (Input.anyKeyDown)
        //    {
        //        hasStarted = true;

        //    }
        //}
        //else
        //{

        if(i < timesArray.Length -1) 
        {
            //
            if (waitToRespawn <= 0 ) 
            {
                    // Spawn Notes
                for(int j =0; j < NotesGeneratorArray.Length;j++) // there's more then one notegenerator
                {                
                    if (j == 0)
                    {
                        var note = NotePool.Instance.Get();

                        note.transform.position = NotesGeneratorArray[j].transform.position;
                        NoteObject noteObject = note.GetComponent<NoteObject>();
                        //Debug.Log(note.name +"Seta vai para a Esquerda");
                        noteObject.direction = NoteObject.Direction.Left;
                        note.gameObject.SetActive(true);
                    }
                    else
                    {
                        var note = NotePool.Instance.Get();

                        note.transform.position = NotesGeneratorArray[j].transform.position;
                        NoteObject noteObject = note.GetComponent<NoteObject>();
                        //Debug.Log(note.name + "Seta vai para a direita");
                        noteObject.direction = NoteObject.Direction.Right;
                        note.gameObject.SetActive(true);
                    }                                     
                }

                    i++;
                    waitToRespawn = float.Parse(timesArray[i], CultureInfo.InvariantCulture);
            }
            else
            {
                    waitToRespawn -= Time.deltaTime;
            }
               
            }
        else
        {
            i = 0;
        }
            
        //}
    }
    public void GetAllFiles()
    {
        //Check if the report directory already exists (if it doesn't, create it)
        if (Directory.Exists(_path))
        {
            string readFromFilePath = Application.streamingAssetsPath + "/songsequence/" + "song" + ".txt";
            ReadFile(readFromFilePath);

        }
        else
        {
            Debug.Log("No reports available.");
            Directory.CreateDirectory(_path);
        }
    }
    public void ReadFile(string readFromFilePath) // Read the files lines
    {
        //Debug.Log(database);
        
        List<string> fileLines = File.ReadAllLines(readFromFilePath).ToList();
        
        //string inputsline = fileLines[0]; // line for the inputs
        //inputArray = inputsline.Split(',');
        string timesline = fileLines[0]; // for now we only have line for the time to appear the buttons
        timesArray = timesline.Split(',');
        waitToRespawn = float.Parse(timesArray[0], CultureInfo.InvariantCulture);
        //Open the file, read it and close it
       // StreamReader streamReader = new StreamReader(filesName[database]);
        //var fileContents = streamReader.ReadToEnd();
        //filescreen.text = fileContents.ToString();
        //streamReader.Close();
        
    }
    
}
