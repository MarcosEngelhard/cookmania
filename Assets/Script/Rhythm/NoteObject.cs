﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class NoteObject : MonoBehaviour // BlasterShootGenericPooled
{
    public enum Direction
    {
        Left, Right
    }
    public Direction direction;
    public bool CanBePressed;
    public float beatTempo;
    //private ParticleSystem particlesSystemButton = new ParticleSystem();

    public LayerMask arrowMask;
    //public KeyCode keyToPress;
    // Start is called before the first frame update
    void Start()
    {
        beatTempo = beatTempo / 60f;
    }

    // Update is called once per frame
    void Update()
    {
        switch (direction)
        {
            case Direction.Left:
                transform.position -= new Vector3(beatTempo * Time.deltaTime, 0f, 0f); // in this case moving down
                if (Input.anyKeyDown)
                {
                    if (CanBePressed)
                    {
                        
                        


                        if ((transform.localPosition.x) > 1.75f)
                        {

                            //GameManager.instance.NormalHit(this);
                        }
                        else if ((transform.localPosition.x) > 0.70f)
                        {

                            //GameManager.instance.GoodHit(this);
                        }
                        else
                        {

                            //GameManager.instance.PerfectHit(this);
                        }
                        //particlesSystemButton.Play();
                    }
                }
                if (transform.localPosition.x < 0)
                {
                    
                    OnBecameInvisible();
                }
                break;
            case Direction.Right:
                transform.position += new Vector3(beatTempo * Time.deltaTime, 0f, 0f); // in this case moving down
                if (Input.anyKeyDown)
                {
                    if (CanBePressed)
                    {
                        
                        


                        if ((transform.localPosition.x) > -1.75f)
                        {

                           // GameManager.instance.NormalHit(this);
                        }
                        else if ((transform.localPosition.x) > -0.70f)
                        {

                           // GameManager.instance.GoodHit(this);
                        }
                        else
                        {

                            //GameManager.instance.PerfectHit(this);
                        }
                        //particlesSystemButton.Play();
                    }
                    
                }
                if (transform.localPosition.x > 0)
                {
                    OnBecameInvisible();
                }


                break;
                
        }
        
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Activator")) // quando fazer trigger, será true
        {
            Debug.Log("Colidei com o Activator");
            CanBePressed = true;
            //particlesSystemButton = collision.gameObject.GetComponent<ParticleSystem>();
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Activator")) // quando fazer trigger, será true
        {
            CanBePressed = false;
            GameManager.instance.NoteMissed();
        }

        
    }

    public void OnBecameInvisible()
    {
        NotePool.Instance.ReturnToPool(this);
    }

}
