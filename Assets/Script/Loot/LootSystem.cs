﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSystem : MonoBehaviour
{
    public ItemToSpawn[] itemToSpawns;
    private float maxRateNumber;
    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < itemToSpawns.Length; i++)
        {

            if(i == 0)
            {
                itemToSpawns[i].minSpawnProb = 0;
                itemToSpawns[i].maxSpawnProb = itemToSpawns[i].spawnRate - 1; // for example 40 - 1 = 39
            }
            else
            {
                itemToSpawns[i].minSpawnProb = itemToSpawns[i - 1].maxSpawnProb + 1; // for example 89 + 1 = 90;
                itemToSpawns[i].maxSpawnProb = itemToSpawns[i].minSpawnProb + itemToSpawns[i].spawnRate - 1; // for example 90 + 30 = 120 - 1 = 119
                
            }
            if(i == itemToSpawns.Length - 1)
            {
                maxRateNumber = itemToSpawns[i].maxSpawnProb;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void Spawner()
    {
        float randomNumber = Random.Range(0, maxRateNumber);
        for(int i = 0; i < itemToSpawns.Length; i++)
        {
            if(randomNumber >= itemToSpawns[i].minSpawnProb && randomNumber <= itemToSpawns[i].maxSpawnProb)
            {
                Instantiate(itemToSpawns[i].item, transform.position, Quaternion.identity);
                break; // exit the loop
            }
        }
    }
}
