﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DieEvent : MonoBehaviour
{
    public delegate void TestDelegate();

    private TestDelegate testDelegateFunction;
    // Start is called before the first frame update
    void Start()
    {
        testDelegateFunction = MyTestDelegateFunction;
        testDelegateFunction += MySecondTestDelegateFunction;

        testDelegateFunction();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void MyTestDelegateFunction()
    {
        Debug.Log("MyTestDelegateFunction");
    }
    private void MySecondTestDelegateFunction()
    {
        Debug.Log("MySecondTestDelegateFunction");
    }
}
