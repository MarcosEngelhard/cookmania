﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;

public class RNG : MonoBehaviour
{
    private int random;
    //private System.Random r;

    public int RandomNumberGenerator(int limit)
    {
        random =  Random.Range(0,limit);
        return random;
    }
}
