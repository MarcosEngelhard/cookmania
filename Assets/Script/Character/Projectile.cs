﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speedProjectile = 10;
    private Rigidbody rb;
    private int amountdamage = 2;
    [SerializeField]private float timeToBeDestroyed = 4f;
    public GameObject CharacterOwner;
    private Character characterOwnerScript;
    private bool isOffScreen;
    [SerializeField] private Image pointTrajectoryPrefab;
    [SerializeField] private GameObject[] points;
    [SerializeField]private int numberOfPoints;
    [SerializeField]private float force = 10f;
    private float positiveForce;
    private float negativeForce;
    [SerializeField]private float timeStamp = 2.5f;
    private float initialTimeStamp;
    /*[HideInInspector]*/public bool isFacingLeft;
    private bool wasuiUpdated;
    [SerializeField] private float OffWidth = 900f;
    [SerializeField] private Canvas CanvasUIElement;
    private float CanvasWidth;
    [SerializeField]private Transform[] RailPositionWarningUI;
    public int Rail;
    private Vector3 currentscale;
    private Vector2 ontheFirstRail = new Vector3(0.40f, 0.40f);
    [SerializeField]private float gravity = 6f;
    [SerializeField]private float fallMultiplier = 8f;
    private SpriteRenderer spriteRenderer;
    private int currentSoartingLayer;
    
    
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        currentSoartingLayer = spriteRenderer.sortingOrder;
        if(CharacterOwner == null)
        {
            CharacterOwner = transform.parent.gameObject;
            characterOwnerScript = CharacterOwner.GetComponent<Character>();
        }
        
        
        gameObject.SetActive(false);
        initialTimeStamp = timeStamp;
        positiveForce = force;
        negativeForce = -force;
        currentscale = transform.localScale;
        
        
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
        if (CharacterOwner.tag == "ProjectileGenerator")
        {
            CanvasWidth = CanvasUIElement.GetComponent<CanvasScaler>().referenceResolution.x;

            //if (Rail == 0)
            //{
            //    transform.localScale = new Vector2(ontheFirstRail.x, ontheFirstRail.y);
            //}
            if (Rail == 0)
            {
                spriteRenderer.sortingOrder -= 2;
                if (CharacterOwner.tag == "ProjectileGenerator")
                {
                    transform.localScale = new Vector2(ontheFirstRail.x, ontheFirstRail.y);

                }


            }
        }

            
    }
    private void OnEnable()
    {
        if (CharacterOwner.TryGetComponent(out Player player))
        {
            Rail = player.GetComponent<Player>().GetPlayerRailPosition();
        }
        transform.parent = null;

        


        if (Rail == 0)
        {
            spriteRenderer.sortingOrder -= 2;
            if (CharacterOwner.tag == "ProjectileGenerator")
            {
                transform.localScale = new Vector2(ontheFirstRail.x, ontheFirstRail.y);
                
            }


        }





        StartCoroutine(BackToOrigin());

        

    }

    // Update is called once per frame
    void Update()
    {
        if (!ButtonsBehaviour.isPaused)
        {
            rb.velocity = transform.right * speedProjectile;
            if(CharacterOwner.tag == "Player")
            {
                if (rb.velocity.y < 0)
                {

                    rb.velocity += Vector3.up * -gravity * (fallMultiplier - 1) * Time.fixedDeltaTime;

                }
            }
            if (CharacterOwner.tag == "ProjectileGenerator")
            {


                Vector3 projectileCoordinates = Camera.main.WorldToScreenPoint(transform.position);
                
                isOffScreen = projectileCoordinates.x > Screen.width;
                pointTrajectoryPrefab.enabled = isOffScreen;

                if (isOffScreen)
                {
                    pointTrajectoryPrefab.transform.position = RailPositionWarningUI[Rail].position;
                }
                else
                {
                   


                }
            }
        }
        
        
        

    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (CharacterOwner.tag != other.gameObject.tag)
        {
            if (other.gameObject.CompareTag("Player")) // hit the player
            {
                other.gameObject.GetComponent<Player>().TakeDamage(amountdamage);

                Destroyed();
            }
        }
        if (CharacterOwner.tag == "Player")
        {
            if (other.gameObject.CompareTag("Enemy")) // hit the enemy
            {
                
                other.gameObject.GetComponent<Enemy>().RemoveShield();
                Destroyed();
            }
        }
    }
    IEnumerator BackToOrigin() // after waiting some time, the projectile is gonna be "destroyed"
    {
        yield return new WaitForSeconds(timeToBeDestroyed);
        Destroyed();

    }
    private void Destroyed() // not exactly destroy, make invisible and put to the owner again
    {

        if(characterOwnerScript != null) // if it's a character put on the attackpoint position and make it father of projectile
        {
            transform.position = characterOwnerScript.attackPoint.position;
            transform.parent = characterOwnerScript.attackPoint.transform;
        }
        else // if not, put on the object position and parent the same
        {
            transform.position = CharacterOwner.transform.position;
            // Projectile parent of the character owner
            transform.parent = CharacterOwner.transform;
        }
        
        this.gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        if (pointTrajectoryPrefab)
        {
            pointTrajectoryPrefab.enabled = false;
        }
        //pointTrajectoryPrefab.enabled = false;
        transform.localRotation = Quaternion.identity;
        spriteRenderer.sortingOrder = currentSoartingLayer;
        wasuiUpdated = false;
        if (currentscale.x == 0 || currentscale.y == 0)
        {
            return;
        }
        if(transform.localScale != currentscale) // put on the normal scale
        {
            transform.localScale = currentscale;
            
        }
        
        

    }
    
    public void Beinghit() // The player has eat the player
    {
        Destroyed();
    }

}
