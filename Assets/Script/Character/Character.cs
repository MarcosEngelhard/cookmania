﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Range(10, 500)] public int maxHealth; //Vida máxima do personagem
    [SerializeField] protected int currentHealth; //Vida atual
    
    protected Action attackFunction; 
    public Transform attackPoint;
    //[SerializeField] public float attackRange;
    [SerializeField] protected LayerMask oponnentsLayers;
    [SerializeField] protected int lightAttackAmount = 20;
    [Range(0f, 20f)] private float nextAttackTime; //Quanto tempo demora entre ataques
    [SerializeField][Range(0.1f, 1f)] public float attackRate = 1f; //Quão rápido o personagem pode atacar de novo
    public CapsuleCollider characterAttackHitbox;
    
    [SerializeField][Range(1f, 250f)] public float walkSpeed; //Velocidade de movimento
    //[SerializeField] private float zSpeed;
    public Rigidbody rbCharacter; //Rigidbody do personagem
    [SerializeField] protected LayerMask floormask;
    [SerializeField] public float jumpHeight; //Altura de pulo
    protected float verticalVelocity;
    [SerializeField][Range(1f, 5f)] public float movementMultiplier;
    protected bool isGrounded; //Se o personagem está no chão
    public BoxCollider bc; //Hitbox do personagem
    public bool isFacingRight; //Se o personagem está olhando para a direita
    protected bool jumpInput; //O personagem pode pular
    [Range(1.5f, 10f)] public float fallMultiplier; //Multiplicador de queda
    protected bool canJump; //Se o jogador pode pular
    [Range(1f, 20f)] public float gravity; //Gravidade

    protected Animator characterAnimator; //Animator associado ao personagem
    public bool canAttack;
    protected float timeBetweenattacks = 0.5f;
    protected bool isAttacking;
    public bool hasShield;  // if the enemy has shield
    private bool isInvincible; //If the character is invincible
    protected bool isCrouching = false;
    protected CameraShaking cameraShaking;
    protected AudioSource audioSource;
    [SerializeField] protected AudioClip playersuccess;
    //public float highscoreMultiplier = 1f;
    protected int highscore = 0;
    [SerializeField] private AudioClip deathAudioClip;
    [SerializeField] public int highscoreNormal = 10;
    //public delegate void WasOnRythm();
    //public WasOnRythm wasOnRythm;
    public int HighScore
    {

        get { return highscore; }
        set { highscore = value; }
    }
    public Animator CharacterAnimator
    {
        get { return characterAnimator; }
    }



    // Starts the basic character parameters
    protected void CharacterStart()
    {
        audioSource = GetComponent<AudioSource>();
        
        currentHealth = maxHealth;
        isFacingRight = true;
        //canJump = true;
        canAttack = true;
        
        
       
        
        isInvincible = false;
        bc = GetComponent<BoxCollider>();
        rbCharacter = GetComponent<Rigidbody>();
        characterAnimator = GetComponent<Animator>();
        cameraShaking = FindObjectOfType<CameraShaking>();
        
    }
    
    protected void PlayerMovement(float LimitToDoRails) // move the player
    {
        if(transform.position.x < LimitToDoRails)
        {
            if (!characterAnimator.GetBool("Walking")) // start walking animation 
            {
                characterAnimator.SetBool("Walking", true);
            }

            rbCharacter.velocity = new Vector3(walkSpeed * Time.fixedDeltaTime, rbCharacter.velocity.y, rbCharacter.velocity.z);

        }
        else
        {
            // Make the character do a little animation
            if (characterAnimator.GetBool("Walking")) // start walking animation 
            {
                characterAnimator.SetBool("Walking", false);
            }
            if(this is Player)
            {
                
                this.GetComponent<Player>().EndScreen();
                CameraShaking.Instance.PutPlayerOnMiddle();
                
            }
            
           
        }

    }


    
    protected void Jump() // Player Jump
    {
        
        if (isGrounded)
        {
            if (jumpInput)
            {
                Debug.Log("Saltar");
                
                verticalVelocity = jumpHeight;
                rbCharacter.velocity = new Vector3(rbCharacter.velocity.x, jumpHeight, rbCharacter.velocity.z) * Time.deltaTime;
                jumpInput = false;
            }
                 
        }
        
        
    }
    
    
    public void Attack() //Ataque do personagem (seja o player ou um tipo de inimigo)
    {
        if (canAttack)
        {
            canAttack = false;
            // Detect enemies in range of attack
            Collider[] hitEnemies = Physics.OverlapSphere(characterAttackHitbox.transform.position, characterAttackHitbox.radius, oponnentsLayers);
            //Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayers);

            // Damage them
            foreach (Collider enemy in hitEnemies)
            {
                bool enemyHasShield = false;
                //Debug.Log("We hit " + enemy.name);
                if (enemy.GetComponent<Character>() != null) // if the object/person that hit was an enemy
                {
                    Character character = enemy.GetComponent<Character>();
                    character.TakeDamage(lightAttackAmount);
                    
                    if (character is Enemy)
                    {
                        //wasOnRythm?.Invoke();
                        //GameManager.instance.IncreaseHighScoreToPlayer(FindObjectOfType<Player>());
                        enemyHasShield = character.hasShield;
                    }

                }
                else // if it was a projectile
                {
                    if(enemy.TryGetComponent(out Projectile projectile))
                    {
                        enemy.GetComponent<Projectile>().Beinghit();
                    }
                    
                }
                if ( enemyHasShield)
                    break;

                this.GetComponent<Player>().IncreaseMultiplierScore();







                audioSource.PlayOneShot(playersuccess);

                cameraShaking.ShakeTheCamera();

            }
            StartCoroutine(AttackAgain()); // Start the courotine
        }
        
    }
    
    public void TakeDamage(int takeamount) // Take damage by an enemy
    {
        if (isInvincible || hasShield || isCrouching)
        {
            
            return;
        }
        currentHealth -= takeamount;
        if(this is Player)
        {
            GetComponent<Player>().healthBar.SetHealth(currentHealth);
        }
        if(currentHealth <= 0)
        {
            GameManager.instance.produceDeathSound(deathAudioClip);
            characterAnimator.SetBool("Dead", true);
            if (this is Player)
            {
                
                GameManager.instance.GameOver();
            }
            else
            {
                bc.enabled = false;

                rbCharacter.isKinematic = true;
            }

            
        }
        else
        {
            characterAnimator.SetBool("Invincibility", true);
            isInvincible = true;
            
        }
    }

    public virtual void BossHealthState(/*int cHP,*/ int damageTaken)
    {
        
    }
    
    

    public void DeactivateInvincibility()
    {
        characterAnimator.SetBool("Invincibility", false);
        if (!canAttack)
        {
            canAttack = true;
        }
        isInvincible = false;
    }
    
    protected IEnumerator AttackAgain()
    {
        yield return new WaitForSeconds(timeBetweenattacks); // wait the this time to attack again
        canAttack = true;
    }
    
    public void Flip(float positionX) // Flip the sprite
    {
        if (positionX > 0 && !isFacingRight)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            isFacingRight = !isFacingRight;
        }
        if (positionX < 0 && isFacingRight)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
            isFacingRight = !isFacingRight;
        }
    }
    
    public void ThrowProjectile(GameObject [] projectiles) // throw projectiles
    {
        if(projectiles != null)
        {
            foreach (GameObject projectile in projectiles)
            {
                if (!projectile.activeInHierarchy)
                {
                    projectile.SetActive(true);
                    StartCoroutine(AttackAgain());
                    return; // causes exit from the current function, while break exit from the loop only
                }

            }
        }
        
        
    }
    public void MoveAutomatically(Transform moveTarget) // When in battle mode, the player moves automatically to be more in the center
    {
        var actualTargetPosition = new Vector3(moveTarget.position.x, transform.position.y, moveTarget.position.z);
        var moveTowards = Vector3.MoveTowards(transform.position, actualTargetPosition, 2 * Time.fixedDeltaTime);
        transform.position = moveTowards;
    }
    
}
