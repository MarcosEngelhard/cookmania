﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermovement : MonoBehaviour
{
    [SerializeField] private float walkSpeed;
    [SerializeField] private float zSpeed;
    private Rigidbody rb;
    private float x;
    private float z;
    private bool Isfacingright;
    //private float minHeight;
    //private float maxHeight;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    private bool jumpInput;
    private bool jumpInputHold;
    private bool jumpInputRelease;
    private bool isGrounded;
    [SerializeField]private LayerMask Floormask;
    private BoxCollider bc;
    [SerializeField] private float jump_height = 20f;
    public float Gravity= 14.0f;
    
    private float verticalVelocity;
    private bool canJump;
    [SerializeField] private float movementMultiplier = 1f;
    public static Playermovement pmovementinstance;
    private float defaultMultiplier;
    
    private void Awake()
    {
        pmovementinstance = this;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Isfacingright = true;
        bc = GetComponent<BoxCollider>();
        canJump = true;
        defaultMultiplier = movementMultiplier;
    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");
        jumpInputHold = Input.GetButtonDown("Jump");
        jumpInput = Input.GetButton("Jump");
        jumpInputRelease = Input.GetButtonUp("Jump");
        Jump();
        if (GameManager.instance.aPerfectNote)
        {
            movementMultiplier = 2.0f;
            Debug.Log("Que perfect timing!!");
        }
    }
    
    private void FixedUpdate()
    {

        Movement();
        

    }
    private void Movement()
    {

        
        rb.velocity = new Vector3(x * walkSpeed * movementMultiplier * Time.fixedDeltaTime, verticalVelocity, z * zSpeed * Time.fixedDeltaTime)/*.normalized*/;
        //rb.velocity = new Vector3(x * walkSpeed * Time.fixedDeltaTime, verticalVelocity, z * zSpeed * Time.fixedDeltaTime).normalized;

        //rb.AddForce(new Vector3(x * walkSpeed * Time.fixedDeltaTime, rb.velocity.y, z * walkSpeed * Time.fixedDeltaTime).normalized, ForceMode.Impulse);
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, walkSpeed); //limitar a velocidade



        if (rb.velocity.y < 0)
        {

            rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.fixedDeltaTime;
        }

        Flip();
        
    }
    
    private void Jump()
    {
        
        isGrounded = Physics.Raycast(bc.bounds.center, Vector3.down, bc.bounds.extents.y + 0.15f , Floormask);


        
        if (isGrounded)
        {
            if (canJump)
            {
                verticalVelocity = -Gravity * Time.fixedDeltaTime;
                if (/*jumpInputHold ||*/ jumpInput)
                {
                    verticalVelocity = jump_height;

                    rb.velocity = new Vector3(0, verticalVelocity, 0);
                    canJump = false;
                }
            }
            
                
            

                
        }
        else
        {
            verticalVelocity -= Gravity * Time.fixedDeltaTime;
        }
        if (jumpInputRelease)
        {
            if (!canJump)
            {
                canJump = true;
            }
            
        }
        
    }
    
    private void Flip() // flip the sprite character according to the direction it's moving
    {
        if (x > 0 && !Isfacingright) 
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            
            Isfacingright = !Isfacingright;
        }
        if (x < 0 && Isfacingright)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
            
            Isfacingright = !Isfacingright;
        }
    }
    
    public void ReturnToDefaultMultiplier()
    {
        if (defaultMultiplier != movementMultiplier)
        {
            movementMultiplier = 1f;
        }

    }
}
