﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField]private int maxHealth = 150;
    private int currentHealth;
    private PlayerAttack playerAttack;
    private Playermovement playerMovement;
    private ExampleEventSystem exampleEventSystem;
    
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        playerAttack = GetComponent<PlayerAttack>();
        playerMovement = GetComponent<Playermovement>();
        exampleEventSystem = GetComponent<ExampleEventSystem>();
        exampleEventSystem.OnSpacePressed += DisableScript;
    }

    private void DisableScript(object sender, EventArgs e)
    {
        this.enabled = false;
        playerMovement.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    /*public void TakeDamage(int takeamount) // Take damage by an enemy
    {
        currentHealth -= takeamount;
        if(currentHealth <= 0)
        {
            Die(); // if the hp goes to 0 (or lower), the player dies
        }
    }*/
    
    private void Die()
    {
        //playerAttack.enabled = false;
        //playerMovement.enabled = false;
        Debug.Log("The player is dead!!");
        exampleEventSystem.DisableScripts();
    }
    private void OnDisable()
    {
        if(exampleEventSystem != null)
        {
            exampleEventSystem.OnSpacePressed -= DisableScript;
        }
        
    }
}
