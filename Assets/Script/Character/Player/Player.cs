﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
public class Player : Character
{
    public KeyCode attackKey; //Tecla para atacar
    public KeyCode saltKey;
    
    //private bool jumpInput; //O personagem pode pular
    public KeyCode jumpKey; //Tecla para pular

    private float posX, posZ;
    public static Player playerinstance;
    [SerializeField] private float[] rails;
    [SerializeField] private int numberlane;

    

    private int defaultAttack;
    [HideInInspector] public bool IsOnBattle;
    
    [SerializeField]private GameObject [] saltObject;
    private Transform moveTarget;
    private Vector3 dir;
    [SerializeField]private Control _control;
    [SerializeField] private float endonRailsX = 63f;
    
    [SerializeField] private AudioClip knifeSound;
    [SerializeField] private BoxCollider crouchCollider;

    [SerializeField] public bool isMobile;
    
    [SerializeField]private Text highscoreText;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private Image finalPanel;
    [SerializeField] private Text finalHighscoreText;
    public HealthBar healthBar;
    private int increasingIndex = 0;
    private bool stoptheIncrease = false;
    [SerializeField]private AudioClip celebrationSFX;

    public delegate void WasOnRythm();
    public WasOnRythm wasOnRythm;

    void Awake()
    {
        playerinstance = this;
        IsOnBattle = false;
       
        finalPanel.gameObject.SetActive(false);
        
      
        
        
        _control = new Control();
        if(_control == null) // If there's no control
        {
            Debug.LogWarning("There's not control!!");
            return;
            
        }
        
        

        audioSource = GetComponent<AudioSource>();
        
    }

    private void JumpInput(InputAction.CallbackContext obj)
    {
        
       jumpInput = true;
        
    }

    private void CrouchInput(InputAction.CallbackContext obj)
    {
        if (ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver)
            return;
        if (isCrouching) // if the player is crouching, don't do that again
            return;
        
        if (isGrounded) // Se estiver no chão
        {
            isCrouching = true;
            bc.enabled = !isCrouching;// if the player is crouching, boxcollider is gone, otherwise, enable it
            characterAnimator.SetBool("Crouch", true);
            
            

        }
        
    }
    

    private void KnifeAttack(InputAction.CallbackContext obj) // With the knife input
    {
        if (ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver)
            return;
        if (canAttack)
        {
            if (IsOnBattle)
            {
                if (!GameManager.instance.aPerfectNote)// atacar on battle
                {
                    return;
                }
                else
                {
                    GameManager.instance.aPerfectNote = false;
                    GameManager.instance.noteEffect = GameManager.NoteEffect.Attack;
                    
                    if(characterAnimator != null)
                    {
                        characterAnimator.SetBool("Attack", true);
                    }
                    //attackFunction();
                }
            }
            else
            {
                if(characterAnimator != null)
                {
                    characterAnimator.SetBool("Attack", true);

                }
                
                //attackFunction();
            }

            
            



        }
    }
    public void EndAttackAnimation() // The end of attack animation return no other
    {
        characterAnimator.SetBool("Attack", false);
        canAttack = true;
    }
    public void EndCrouchAnimation()
    {
        Debug.Log("Fim do Agachar!!");
        isCrouching = false;
        bc.enabled = !isCrouching;
        
        characterAnimator.SetBool("Crouch", false);
    }
    public void MediumAttackAnimation()
    {
        attackFunction();
        audioSource.PlayOneShot(knifeSound);
    }
    

    private void Salt(InputAction.CallbackContext obj) // Salt Input
    {
        if (ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver)
            return;


        if (canAttack)
        {
            if (IsOnBattle)
                if (!GameManager.instance.aPerfectNote)
                    return; // if on battle didn't point the beat, it's not gonna work
            ThrowProjectile(saltObject); // Throw Salt

        }
        
    }

    private void GoDown(InputAction.CallbackContext obj)
    {
        if (IsOnBattle || ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver) // In battle you cannot chance the rail
            return;
        if (numberlane >= rails.Length -1) // 2 is the lowest Rail
        {
            return;
        }
        else
        {
            if(spriteRenderer != null)
            {
                spriteRenderer.sortingOrder += 2;
            }
            
            numberlane++;
            // Get the delta position
            transform.position = new Vector3(transform.position.x, transform.position.y, rails[numberlane]);
            


        }
    }

    private void GoUp(InputAction.CallbackContext obj)
    {
        
        if (IsOnBattle || ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver) // In battle you cannot chance the rail
            return;
        
        if (numberlane <= 0) // on is the highest lane
        {

            return;
        }
        else
        {
            if(spriteRenderer != null)
            {
                spriteRenderer.sortingOrder -= 2;
            }
            
            numberlane--;

            transform.position = new Vector3(transform.position.x, transform.position.y, rails[numberlane]);
            




        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CharacterStart();
        transform.position = new Vector3(transform.position.x, transform.position.y, rails[numberlane]);
        attackFunction = Attack;
        healthBar = FindObjectOfType<HealthBar>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        healthBar.SetMaxHealth(currentHealth);
        _control.Player.Up.performed += GoUp;
        _control.Player.Up.Enable();
        _control.Player.Down.performed += GoDown;
        _control.Player.Down.Enable();
        _control.Player.Salt.performed += Salt;
        _control.Player.Salt.Enable();
        _control.Player.Knife.performed += KnifeAttack;
        _control.Player.Knife.Enable();
        //_control.Player.Crouch.performed += CrouchInput;
        //_control.Player.Crouch.Enable();
        //_control.Player.Jump.performed += JumpInput;
        //_control.Player.Jump.Enable();

    }

    // Update is called once per frame
    void Update()
    {
        if (!ButtonsBehaviour.isPaused)
        {
            posX = Input.GetAxis("Horizontal");
            
            //Jump(); // saltar
            highscoreText.text = highscore.ToString();
            //if (isMobile)
            //{
         
                
            //    if (CrossPlatformInputManager.GetButtonDown("Canela"))
            //    {
            //        ThrowProjectile(saltObject); // Throw Salt
            //    }
            //    if (CrossPlatformInputManager.GetButtonDown("Knife"))
            //    {
            //        if (canAttack)
            //        {


            //            characterAnimator.SetBool("Attack", true);
            //        }
            //    }
            //}

        }
        
        
    }
    
    private void FixedUpdate()
    {
        if (GameManager.instance.IsGameOver || GameManager.instance.IsCompleted)
        {
            return;
        }
        if (!ButtonsBehaviour.isPaused)
        {
            
             isGrounded = Physics.Raycast(bc.bounds.center, Vector3.down, bc.bounds.extents.y + 0.15f, floormask); // verificar se está no chão ou não
            
            
            
            if (rbCharacter.velocity.y < 0 && !isGrounded && !isCrouching)
            {

                rbCharacter.velocity += Vector3.up * -gravity * (fallMultiplier - 1) * Time.fixedDeltaTime;

            }

            if (!IsOnBattle) // If the player is not in combat he can move freely
            {
                PlayerMovement(endonRailsX);
                
                if (!isFacingRight)
                {
                    transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    isFacingRight = !isFacingRight;
                }


            }
            else
            {
                if (Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(moveTarget.position.x, 0, moveTarget.position.z)) > 1f) // if the 
                {
                    characterAnimator.SetBool("Walking", true);
                    MoveAutomatically(moveTarget);
                }
                else
                {
                    characterAnimator.SetBool("Walking", false);
                    
                }
                Flip(posX); // Flip the sprite of the character
            }
        }
        
        

        
    }

    public void EndScreen() // Show the end screen panel with the final highscore
    {
        if (!stoptheIncrease)
        {
            if (!finalPanel.gameObject.activeInHierarchy)
            {
                GameManager.instance.IncreaseHighScoreToPlayer(this);
                audioSource.clip = celebrationSFX;
                audioSource.Play(); // brom is celebrating
                audioSource.loop = true;
                characterAnimator.SetBool("Dancing", true);
                highscoreText.gameObject.SetActive(false);
                finalPanel.gameObject.SetActive(true);
                finalHighscoreText.text = "Final Highscore:  " + highscore.ToString("0");
                DisableInputKeys();

            }
            StartCoroutine(ExtraPoints());
            stoptheIncrease = true;
            
        }
        
        
        
    }
    IEnumerator ExtraPoints()
    {
        yield return new WaitForSeconds(2f);
        for(int i = 0; i < currentHealth; i++)
        {
            yield return new WaitForSeconds(0.02f); // be smooth instead "appear at once"
            highscore += 10;
            finalHighscoreText.text = "Final Highscore:  " + highscore.ToString("0");
        }
    }

    public void GetSpotTarget(Transform moveTarget)
    {
        this.moveTarget = moveTarget;
    }
    public float [] GetRails()
    {
        return rails; 
    }
    public BoxCollider GetStandardBoxCollider()
    {
        return bc;
    }
    public Collider GetCrouchCollider()
    {
        return crouchCollider;
    }
    public int GetPlayerRailPosition()
    {
        return numberlane;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SmartieAzul")) // if it's on good 
        {
            //wasOnRythm = GameManager.instance.NormalHit;
            other.gameObject.GetComponent<Smartie>().PlayerCollision();
        }
        else if (other.gameObject.CompareTag("SmartieVerde")) // if it's on great
        {
            other.gameObject.GetComponent<Smartie>().PlayerCollision();
            //wasOnRythm = GameManager.instance.GoodHit;
        }
        else if (other.gameObject.CompareTag("SmartieYellow")) // if it's on perfect
        {
            other.gameObject.GetComponent<Smartie>().PlayerCollision();
            //wasOnRythm = GameManager.instance.PerfectHit;
        }
        
    }
    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("SmartieYellow")) // make the delegate empty
        {
            wasOnRythm = null;
        }
    }
    public void IncreaseMultiplierScore()
    {
        wasOnRythm?.Invoke();
    }
    public void DisableInputKeys()
    {
        _control.Player.Up.performed -= GoUp;
        _control.Player.Up.Disable();
        _control.Player.Down.performed -= GoDown;
        _control.Player.Down.Disable();
        _control.Player.Salt.performed -= Salt;
        _control.Player.Salt.Disable();
        _control.Player.Knife.performed -= KnifeAttack;
        _control.Player.Knife.Disable();
        //_control.Player.Crouch.performed += CrouchInput;
        //_control.Player.Crouch.Enable();
        _control.Player.Jump.performed -= JumpInput;
        _control.Player.Jump.Disable();
    }

}
