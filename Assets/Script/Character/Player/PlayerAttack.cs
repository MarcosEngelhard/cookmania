﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private Action AttackFunction;
    
    //private Animator animator;
    [SerializeField] private Transform attackPoint;
    [SerializeField] public float attackRange = 0.5f;
    [SerializeField] private LayerMask enemyLayers;
    [SerializeField] private int lightAttackAmount = 20;

    [SerializeField] private float attackRate = 1f;
    private float nextAttackTime;
    private ExampleEventSystem exampleEventSystem;

    // Start is called before the first frame update
    void Start()
    {
        AttackFunction = KnifeAttack;
        exampleEventSystem = GetComponent<ExampleEventSystem>();
        exampleEventSystem.OnSpacePressed += DisableThisScript;
        //animator = GetComponent<Animator>();
    }

    private void DisableThisScript(object sender, EventArgs e)
    {
        this.enabled = false;
        exampleEventSystem.OnSpacePressed -= DisableThisScript;


    }

    // Update is called once per frame
    void Update()
    {
        HandleAttack();
        //if(Time.time >= nextAttackTime) // once reached the attacktime the player can attack
        //{
        //    if (Input.GetKeyDown(KeyCode.F))
        //    {
        //        Attack();
        //        nextAttackTime = Time.time + 1f / attackRate;
        //    }
        //}
        
    }
    
    void HandleAttack()
    {
        if (Time.time >= nextAttackTime) // once reached the attacktime the player can attack
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                AttackFunction();
                nextAttackTime = Time.time + 1f / attackRate;
            }
        }
    }
    
    void KnifeAttack()
    {
        // Play an attack animation
        //animator.SetTrigger("Attack");

        // Detect enemies in range of attack
        Collider[] hitEnemies = Physics.OverlapSphere(attackPoint.position, attackRange, enemyLayers);

        // Damage them
        foreach(Collider enemy in hitEnemies)
        {
            Debug.Log("We hit "+  enemy.name);
            enemy.GetComponent<EnemyHealth>().TakeDamage(lightAttackAmount);
        }
    }
    
    private void OnDrawGizmosSelected() // See on UNity
    {
        if (attackPoint == null)
            return;

        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
    //private void SetUseCutelo()
    //{
    //    AttackFunction = KnifeAttack;
    //}
}
