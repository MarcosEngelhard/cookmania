﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class EnemyAttack : MonoBehaviour, IEnemyAttack
{
    [SerializeField]private Transform enemyAttackPoint;
    [SerializeField]private float attackRange = 2f;
    [SerializeField] private LayerMask playerMask;
    private bool canAttack;
    //private Animator animator;
    //private AnimationClip [] animationClip;
    private float timeBetweenattacks = 0.5f;
    public bool isAttacking;
    
    [SerializeField] private int attackAmount = 20;
    [SerializeField] private Enemymovement enemymovement;
    

    // Start is called before the first frame update
    void Start()
    {
        
        // assign the animator and see the attack animationclip
        //animator = GetComponent<Animator>();

        //animationClip = animator.runtimeAnimatorController.animationClips;

        //foreach(AnimationClip clip in animationClip)
        //{
        //    if(clip.name == "attack")
        //    {
        //        timeBetweenattacks = clip.length + 0.5f;
        //    }
        //}
        enemymovement = GetComponent<Enemymovement>();
        canAttack = true;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void TriggerAttackanimation()
    {
        if (canAttack)
        {
            if (enemymovement.enabled)
            {
                enemymovement.enabled = false;
            }

            StartCoroutine(AttackAgain());
            canAttack = false;

            
            // Play the attack animation
            //animation.setTrigger("attack");
            // Detect player(s) in range of attack
            Collider[] playermask = Physics.OverlapSphere(enemyAttackPoint.position, attackRange, playerMask);

            // Damage the player
            foreach (Collider player in playermask)
            {
                Debug.Log("We hit " + player.name);
                if(player.GetComponent<PlayerHealth>() != null)
                {
                    //player.GetComponent<PlayerHealth>().TakeDamage(attackAmount);
                }
                //enemy.GetComponent<EnemyHealth>().TakeDamage(lightAttackAmount);
            }
        }
        
    }
    public void Disable()
    {
        this.enabled = false;
    }
    //animator event
    //private void AttackEnd()
    //{
    //    // Detect player(s) in range of attack
    //    Collider[] hitEnemies = Physics.OverlapSphere(enemyAttackPoint.position, attackRange, playerMask);

    //    // Damage them
    //    foreach (Collider enemy in hitEnemies)
    //    {
    //        Debug.Log("We hit " + enemy.name);
    //        //enemy.GetComponent<EnemyHealth>().TakeDamage(lightAttackAmount);
    //    }
    //}
    private void OnDrawGizmosSelected() // See on UNity
    {
        if (enemyAttackPoint == null)
            return;
        
        Gizmos.DrawWireSphere(enemyAttackPoint.position, attackRange);
    }
    IEnumerator AttackAgain()
    {
        
        
        yield return new WaitForSeconds(timeBetweenattacks);
        enemymovement.enabled = true;
        Debug.Log("Movimenta outra vez!!");
        canAttack = true;
        
    }
    
    
}
