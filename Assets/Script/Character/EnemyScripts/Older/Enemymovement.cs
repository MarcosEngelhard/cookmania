﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemymovement : MonoBehaviour
{
    private enum State
    {
        Moving, Chase, Attack,
    }
    [SerializeField] private State state;
    [SerializeField] private float speed;
    private float WaitTime;
    public float startWaitTime;

    public Transform moveSpot;
    [SerializeField] private float MinX;
    [SerializeField] private float MaxX;
    [SerializeField] private float MinZ;
    [SerializeField] private float MaxZ;
    private GameObject target;
    [SerializeField] private float distanceToChase = 25f;
    [SerializeField] private float stopingDistance = 0.8f;
    [SerializeField] private float StopToChase = 50f;
    private bool isWaiting;
    private IEnemyAttack enemyAttack;
    private EnemyHealth enemyHealth;
    private bool isFacingRight;
    private ExampleEventSystem exampleEventSystem;
    
    // Start is called before the first frame update
    void Start()
    {

        state = State.Moving;
        WaitTime = startWaitTime;
        moveSpot.position = new Vector3(UnityEngine.Random.Range(MinX, MaxX), moveSpot.position.y, UnityEngine.Random.Range(MinZ, MaxZ));
        target = FindObjectOfType<Playermovement>().gameObject;
        enemyAttack = GetComponent<IEnemyAttack>();
        enemyHealth = GetComponent<EnemyHealth>();
        isWaiting = false;
        isFacingRight = false;
        exampleEventSystem = GetComponent<ExampleEventSystem>();
        exampleEventSystem.OnSpacePressed += DisableThisScript;
    }
    private void DisableThisScript(object sender,EventArgs e)
    {
        this.enabled = false;
        exampleEventSystem.OnSpacePressed -= DisableThisScript;
    }

    // Update is called once per frame
    void Update()
    {
        if(!enemyHealth.IsDead)
        switch (state)
        {
            case State.Moving:
                MovingToSpot();
                break;
            case State.Chase:
                ChaseThePlayer();
                break;
            case State.Attack:
                Attack();
                break;
        }
        
        
        


    }
    private void MovingToSpot()
    {
        

        if (Vector3.Distance(transform.position, moveSpot.position) < 0.7f) // if is nearby the destiny, chance the waypoint to another place
        {
            if (!isWaiting)
            {
                isWaiting = true;
                transform.position += Vector3.zero;
                StartCoroutine(GoToNextSpot());
            }
            
            
        }
        else
        {

            transform.position = Vector3.MoveTowards(transform.position, new Vector3(moveSpot.position.x,transform.position.y,moveSpot.position.z), speed * Time.deltaTime); // Move to the spot
            Flip(moveSpot);
        }
        if (Vector3.Distance(transform.position, target.transform.position) < distanceToChase)
        {
            
            state = State.Chase;
        }
    }
    
    private void ChaseThePlayer()
    {
        if (Vector3.Distance(transform.position, target.transform.position) < stopingDistance)
        {
            transform.position += Vector3.zero;
            state = State.Attack;
        }
        else if (Vector3.Distance(transform.position, target.transform.position) > StopToChase) // Move through waypoints
        {
            state = State.Moving;
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x,transform.position.y,target.transform.position.z), speed * Time.deltaTime);
            Flip(target.transform);
        }
    }
    
    IEnumerator GoToNextSpot()
    {
        yield return new WaitForSeconds(startWaitTime);
        moveSpot.position = new Vector3(UnityEngine.Random.Range(MinX, MaxX), transform.position.y, UnityEngine.Random.Range(MinZ, MaxZ));
        WaitTime = startWaitTime;
        isWaiting = false;
    }
    
    public void Attack()
    {
        if (Vector3.Distance(transform.position, target.transform.position) > stopingDistance)
        {
            state = State.Chase;
        }
        else
        {
            enemyAttack.TriggerAttackanimation();
            Debug.Log("Olá!!");
        }
        
        
    }
    private void Flip(Transform destination)// Flip the sprite rotation
    {
        if(destination.transform.position.x < transform.position.x && isFacingRight)
        {
            isFacingRight = false;
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        if (destination.transform.position.x > transform.position.x && !isFacingRight)
        {
            isFacingRight = true;
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
        
        

}
