﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private int maxhealth = 100;
    private int currenthealth;
    private Collider collider;
    private Rigidbody rb;
    public bool IsDead;
    private IEnemyAttack enemyAttack;
    private Component component;
    private Enemymovement enemymovement;
    private ExampleEventSystem exampleEventSystem;
    //[SerializeField] private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        currenthealth = maxhealth;
        IsDead = false;
        collider = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        enemyAttack = GetComponent<IEnemyAttack>();
        
        enemymovement = GetComponent<Enemymovement>();
        exampleEventSystem = GetComponent<ExampleEventSystem>();
        exampleEventSystem.OnSpacePressed += DisableScript;
    }

    private void DisableScript(object sender, EventArgs e)
    {
        this.enabled = false;
        enemymovement.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //animator = GetComponent<Animator>();
    }
    public void TakeDamage(int amount)
    {
        currenthealth -= amount;
        // falta tocar a animação de se magoar ( hurt animation)
        // animator.setTrigger("Hurt");
        if(currenthealth <= 0)
        {
            Die();
        }
    }
    void Die()
    {
        // Die animation
        Debug.Log("Enemy died!");
        //animator.SetBool("IsDead, true");
        //Disable the enemy
        collider.enabled = false;
        rb.isKinematic = true;
        enemyAttack.Disable();

        //enemyAttack.Disable();
        enemymovement.enabled = false;
        this.enabled = false;
        IsDead = true;
        //Destroy the player
        exampleEventSystem.DisableScripts();
        Destroy(this.gameObject, 4f);


    }
}
