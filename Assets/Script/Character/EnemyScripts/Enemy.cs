﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : Character
{
    protected enum State
    {
        Moving, Chase, Attack, Idle, Ranged
    }
    protected enum EnemyType
    {
        Brocco, Carrot
    }
    protected EnemyType enemyType;
    private float waitTime;
    public float startWaitTime;

    public Transform moveSpot;
    [SerializeField] private float minX;
    [SerializeField] private float maxX;
    [SerializeField] private float minZ = -9.5f;
    [SerializeField] private float maxZ = -2.6f;
    protected GameObject target;
    [SerializeField] private float distanceToChase;
    [SerializeField] protected float stopingDistance;
    [SerializeField] private float stopToChase;
    private bool isWaiting;
    protected bool isOnBeat;
    //private bool JumpEnabled;
    [SerializeField] private float XForce = 15f;
        
    [SerializeField] private GameObject shieldGameObject;
    //[SerializeField]private bool makePartOftheBattle;
    [SerializeField] protected Canvas worldSpaceCanvas;

    [SerializeField] protected Image knifeIcon;

    [SerializeField] protected Image cinnamonIcon;



    //private ExampleEventSystem exampleEventSystem;

    // Starts the enemy parameters
    protected void EnemyStart()
    {
        CharacterStart();
        waitTime = startWaitTime;
        if(moveSpot != null)
        {
            moveSpot.position = new Vector3(UnityEngine.Random.Range(minX, maxX), moveSpot.position.y, UnityEngine.Random.Range(minZ, maxZ));
        }
        target = FindObjectOfType<Player>().gameObject;
        isWaiting = false;
        isFacingRight = true;
        if(this is Carrot) // Defining what enemy type is
        {
            enemyType = EnemyType.Carrot;
        }
        else
        {
            enemyType = EnemyType.Brocco;
        }
        isOnBeat = false;
        if (hasShield)
        {

            shieldGameObject.SetActive(true);
            cinnamonIcon.gameObject.SetActive(true);
        }
        else
        {
            shieldGameObject.SetActive(false);
            cinnamonIcon.gameObject.SetActive(false);
        }
        
        
    }
    public void RemoveShield() // Removing Shield by the Salt
    {
        if (hasShield)
        {

            shieldGameObject.SetActive(false);
            cinnamonIcon.gameObject.SetActive(false);
            hasShield = false;
        }
    }

    protected State MovingToSpot()
    {
        if (Vector3.Distance(transform.position, moveSpot.position) < 2f) // if is nearby the destiny, chance the waypoint to another place
        {
            if (!isWaiting)
            {
                isWaiting = true;
                transform.position += Vector3.zero;
                StartCoroutine(GoToNextSpot());
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, 
                new Vector3(moveSpot.position.x,transform.position.y,moveSpot.position.z), 
                walkSpeed * Time.deltaTime); // Move to the spot
            //Movement(moveSpot.position.x, moveSpot.position.z);
        }
        if(target != null)
        {
            if (Vector3.Distance(transform.position, target.transform.position) < distanceToChase)
            {
                return State.Chase;
            }
        }
        return State.Moving;
        

    }
    
    IEnumerator GoToNextSpot()
    {
       // Generate another position for moveSport
        yield return new WaitForSeconds(startWaitTime);
        moveSpot.position = new Vector3(UnityEngine.Random.Range(minX, maxX), transform.position.y, UnityEngine.Random.Range(minZ, maxZ));
        waitTime = startWaitTime;
        isWaiting = false;
    }
    
    protected State ChaseThePlayer()
    {
        //if (Vector3.Distance(transform.position, target.transform.position) < stopingDistance) // if the enemy is too close
        //{
        //    transform.position += Vector3.zero;
        //    return State.Attack;
        //}
        if (Vector3.Distance(transform.position, target.transform.position) > stopToChase) // Move through waypoints
        {
            return State.Moving;
        }
        else
        {
            EnemyJump();
            transform.position = Vector3.MoveTowards(transform.position, 
                new Vector3(target.transform.position.x,transform.position.y,target.transform.position.z), 
                walkSpeed * Time.deltaTime);
            //Movement(target.transform.position.x, target.transform.position.z);
        }
        
        return State.Chase;
    }
    
    protected void FlipEnemy(Transform target)
    {
        if (transform.position.x < target.position.x && !isFacingRight)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            isFacingRight = !isFacingRight;
        }
        if (transform.position.x > target.position.x && isFacingRight)
        {
            transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
            isFacingRight = !isFacingRight;
        }
    }
    public void AttackOnBeat() // This is gonna help to enemy can hit only on beat
    {
        if (isOnBeat)
            return;
        isOnBeat = true;
    }
    public void EnemyJump()
    {
        isGrounded = Physics.Raycast(bc.bounds.center, Vector3.down, bc.bounds.extents.y + 0.15f, floormask); // verificar se está no chão ou não
        if(isGrounded)
        {

            rbCharacter.AddForce(Vector3.up * jumpHeight ,ForceMode.Impulse);
            Vector3 direction =  target.transform.position - transform.position;
            if(Mathf.Abs(target.transform.position.x - transform.position.x) > 1 || Mathf.Abs(target.transform.position.z- transform.position.z) > 1)
            {
                
                direction.x = transform.position.x > target.transform.position.x ? -XForce : XForce; // if it's true :: if it's false
            }
            else
            {
                direction.x *= 0.45f;
            }
            characterAnimator.SetBool("Jump", true);
            
            


                       
            rbCharacter.AddForce(direction , ForceMode.Impulse);
                        
        }
    }
    
}
