﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brocco : Enemy
{
    [SerializeField] protected State state;
    [SerializeField] private ResetHighscoreTrigger resetHighscore;
    private float[] playerRails;
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }






    // Start is called before the first frame update
    void Start()
    {
        playerRails = FindObjectOfType<Player>().GetRails();
        if(transform.position.z == playerRails[0])
        {
            spriteRenderer.sortingOrder -= 2;
        }

        state = State.Moving;
        worldSpaceCanvas.worldCamera = Camera.main;
        
        EnemyStart();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameManager.instance.IsGameOver || GameManager.instance.IsCompleted)
        {
            //if(!characterAnimator.enabled)
                characterAnimator.enabled = false;

            return;
        }
        if (!ButtonsBehaviour.isPaused )
        {
            switch (state)
            {
                case State.Moving:
                    if (moveSpot != null)
                    {
                        state = MovingToSpot();
                        FlipEnemy(moveSpot);
                    }
                    else
                    {
                        state = State.Chase;
                    }
                    break;
                case State.Chase:

                    if (Vector3.Distance(transform.position, target.transform.position) < stopingDistance) // if the enemy is too close
                    {
                        transform.position += Vector3.zero;
                        state = State.Attack;
                    }
                    else
                    {
                        if (target != null)
                        {
                            if (rbCharacter.velocity.y < 0)
                            {

                                rbCharacter.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.fixedDeltaTime;
                            }
                            if (isOnBeat)
                            {

                                state = ChaseThePlayer();
                                FlipEnemy(target.transform);

                                isOnBeat = false;
                            }
                            else if(isGrounded)
                            {
                                characterAnimator.SetBool("Jump", false);
                            }

                        }
                        else
                        {
                            target = GetComponent<Player>().gameObject;
                        }
                    }

                    break;
                case State.Attack:
                    if (target != null)
                    {
                        if (Vector3.Distance(transform.position, target.transform.position) > stopingDistance)
                        {
                            characterAnimator.SetBool("Attack", false);
                            state = State.Chase;
                        }
                        else
                        {
                            if (isOnBeat)
                            {
                                if (canAttack)
                                {

                                    FlipEnemy(target.transform);
                                    characterAnimator.SetBool("Attack", true);
                                    //attackFunction();

                                }
                            }
                            else
                            {
                                characterAnimator.SetBool("Attack", true);
                                //attackFunction();
                            }


                            //HandleAttack();
                        }
                    }
                    else
                    {
                        characterAnimator.SetBool("Attack", false);
                        state = State.Moving;
                    }
                    break;
            }
        }
        

    }
    public void EndJumpAnimation()
    {
        characterAnimator.SetBool("Jump", false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Player player = collision.gameObject.GetComponent<Player>();
            Physics.IgnoreCollision(bc, player.GetStandardBoxCollider()); // avoid the enemy drag the player
            Physics.IgnoreCollision(bc, player.GetCrouchCollider()); // avoid the enemy drag the player
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if(other.transform.position.z != transform.position.z)
            {
                bool hasPlayedPassed = resetHighscore.PullTrigger(other);
                if (hasPlayedPassed)
                {
                    Player player = other.gameObject.GetComponent<Player>();

                    //GameManager.instance.CurrentMultiplier = 1;


                }
            }
            
        }
        
    }
    private void AttackPlayer() // by the Cerry animation
    {
        Collider[] hitPlayer = Physics.OverlapSphere(characterAttackHitbox.transform.position, characterAttackHitbox.radius, oponnentsLayers);
        if(hitPlayer != null)
        {
            foreach(Collider playerToHit in hitPlayer)
            {
                Player playerScript = playerToHit.GetComponent<Player>();
                playerScript.TakeDamage(lightAttackAmount);
                //playerScript.IncreaseHighScore();
                GameManager.instance.IncreaseHighScoreToPlayer(playerScript);
                //playerScript.highscoreMultiplier = 1f;
                audioSource.PlayOneShot(playersuccess);

                cameraShaking.ShakeTheCamera();
                StartCoroutine(AttackAgain());
            }
        }
        
    }
    public void BeingDestroyed() // called by the animator
    {
        this.gameObject.SetActive(false);
    }
    
    

}
