﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carrot : Enemy
{
    [SerializeField] protected State state;
    public GameObject[] ThrowCarrots;
    
    // Start is called before the first frame update
    void Start()
    {
        EnemyStart();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Moving:
                if (moveSpot != null)
                {
                    state = MovingToSpot();
                    FlipEnemy(moveSpot);
                }
                else
                {
                    state = State.Chase;
                }
                break;
            case State.Chase:

                if (target != null)
                {
                    state = ChaseThePlayer();
                    FlipEnemy(target.transform);
                }
                
                break;
            case State.Attack:
                if (target != null)
                {
                    if (Vector3.Distance(transform.position, target.transform.position) > stopingDistance)
                    {
                        state = State.Chase;
                    }
                    else
                    {
                        if((int)transform.position.z == (int)target.transform.position.z)
                        {
                            if (canAttack)
                            {
                                ThrowProjectile(ThrowCarrots);

                            }
                        }
                        

                        
                    }
                }
                else
                {
                    state = State.Moving;
                }
                break;
        }
    }
    
    protected void AttackProjectile()
    {
        canAttack = false;
        
        StartCoroutine(AttackAgain());
    }
}
