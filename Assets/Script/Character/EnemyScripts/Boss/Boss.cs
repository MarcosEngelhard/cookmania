﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : Enemy
{
    protected enum HealthThreshold
    {
        FullHealth, HighHealth, HalfHealth, LowHealth
    }

    private HealthThreshold ht;
    
    
    [SerializeField] private int shieldHealth = 150; // shield hp
    
    
    // Start is called before the first frame update
    protected void BossStart()
    {
        //EnemyStart();
        rbCharacter = GetComponent<Rigidbody>();
        ht = HealthThreshold.FullHealth;
        hasShield = true;
        currentHealth = maxHealth;
    }

    public void BossBehaviour()
    {
        if (hasShield)
        {
            switch (ht)
            {
                case HealthThreshold.FullHealth:
                    FullHealthBehaviour();
                    break;
                case HealthThreshold.HighHealth:
                    HighHealthBehaviour();
                    break;
                case HealthThreshold.HalfHealth:
                    HalfHealthBehaviour();
                    break;
                case HealthThreshold.LowHealth:
                    LowHealthBehaviour();
                    break;
            }
        }
        /*else
        {
            //characterAnimator.SetBool("Stunned", true);
        }*/
    }
    
    public virtual void FullHealthBehaviour()
    {
        
    }

    public virtual void HighHealthBehaviour()
    {
        
    }
    
    public virtual void HalfHealthBehaviour()
    {
        
    }
    
    public virtual void LowHealthBehaviour()
    {
        
    }

    public override void BossHealthState(/*int cHP,*/ int damageTaken)
    {
        if (!hasShield)
        {
            currentHealth -= damageTaken;
            if(currentHealth < 0)
            {
                gameObject.SetActive(false);
            }
            //switch (ht)
            //{
            //    case HealthThreshold.FullHealth:
            //        if (cHP <= (maxHealth * 0.75))
            //        {
            //            ht = HealthThreshold.HighHealth;
            //        }

            //        break;
            //    case HealthThreshold.HighHealth:
            //        if (cHP <= (maxHealth * 0.5))
            //        {
            //            ht = HealthThreshold.HalfHealth;
            //        }

            //        break;
            //    case HealthThreshold.HalfHealth:
            //        if (cHP <= (maxHealth * 0.25))
            //        {
            //            ht = HealthThreshold.LowHealth;
            //        }

            //        break;
            //    case HealthThreshold.LowHealth:
            //        break;
            //}
        }
        else
        {
            shieldHealth -= damageTaken;
        }
    }

    public void DeactivateAnimation(string animationParameter)
    {
        characterAnimator.SetBool(animationParameter, false);
    }
}
