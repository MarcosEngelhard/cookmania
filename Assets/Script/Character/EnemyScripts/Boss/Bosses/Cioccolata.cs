﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cioccolata : Boss
{
    private RNG rng;
    private Transform mainCamera;
    [SerializeField]private GameObject leftHand;
    [SerializeField]private GameObject rightHand;
    private Rigidbody lHandrb;
    private Rigidbody rHandrb;
    private BoxCollider lHandbc;
    private BoxCollider rHandbc;
    private CioccolataHands lcioccolataHands;
    private CioccolataHands rcioccolataHands;
    [SerializeField]private Transform lHandOriginalPos;
    [SerializeField]private Transform RHandOriginalPos;
    [SerializeField] private GameObject slapHighlight; // highlights where the boss is going to hit
    [SerializeField] private GameObject passHighlight;
    private float[] railsPosition;
    private float toPosition;
    private Player player;
    private bool isBossAttacking = false;
    [SerializeField] private float handSpeed = 12f;
    private delegate void Currentattack();
    private Currentattack currentattack;
    private GameObject currentHand;
    private Rigidbody currentHandRb;
    private BoxCollider currentHandBc;
    private CioccolataHands currentCioccolataHands;
    private Transform currentOriginalPosition;
    private delegate void ReturnToInitialPosition();
    private ReturnToInitialPosition ReturnTo;


    // Start is called before the first frame update
    void Start()
    {
        
        rng = GetComponent<RNG>();
        mainCamera = Camera.main.GetComponent<Camera>().transform;
        player = FindObjectOfType<Player>();
        slapHighlight.SetActive(false);
        passHighlight.SetActive(false);
        lHandrb = leftHand.GetComponent<Rigidbody>();
        rHandrb = rightHand.GetComponent<Rigidbody>();
        lHandbc = leftHand.GetComponent<BoxCollider>();
        rHandbc = rightHand.GetComponent<BoxCollider>();
        lcioccolataHands = leftHand.GetComponent<CioccolataHands>();
        rcioccolataHands = rightHand.GetComponent<CioccolataHands>();
        railsPosition = player.GetRails();
        currentattack = null;
        BossStart();
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.position = new Vector3(mainCamera.position.x, transform.position.y, transform.position.z);
        currentattack?.Invoke(); // if currentattack is not null invoke it
        Debug.Log(currentattack);
        ReturnTo?.Invoke();
        Debug.Log(ReturnTo);
        BossBehaviour();
    }
    
    public override void FullHealthBehaviour() // Override the boss script
    {
        BasicBehaviour();
    }

    public override void HighHealthBehaviour()
    {
        BasicBehaviour();
    }
    
    public override void HalfHealthBehaviour()
    {
        BasicBehaviour();
    }
    
    public override void LowHealthBehaviour()
    {
        switch (rng.RandomNumberGenerator(3))
        {
            case 0 :
                

                
                //characterAnimator.SetBool("Attack1", true);
                break;
            case 1 :
                //characterAnimator.SetBool("Attack2", true);
                break;
            case 2 :
                //characterAnimator.SetBool("Attack3", true);
                break;
        }
    }

    public void BasicBehaviour()
    {
        if (!isBossAttacking) // in case boss are not attacking
        {
            int numberofRail = player.GetPlayerRailPosition();
            switch (rng.RandomNumberGenerator(2))
            {
                case 0:
                    currentattack = BasicAttack;
                    currentHand = rightHand;
                    currentHandRb = rHandrb;
                    currentHandBc = rHandbc;
                    currentCioccolataHands = rcioccolataHands;
                    isBossAttacking = true;


                    toPosition = railsPosition[numberofRail];
                    //characterAnimator.SetBool("Attack1", true);
                    break;
                case 1:
                    currentattack = BasicAttack;
                    isBossAttacking = true;
                    currentHand = leftHand;
                    currentHandRb = lHandrb;
                    currentHandBc = lHandbc;
                    currentCioccolataHands = lcioccolataHands;
                    toPosition = railsPosition[numberofRail];
                    break;
            }
        }
        
        
    }
    private void BasicAttack()
    {
        Vector3 targetPosition = new Vector3(currentHand.transform.position.x, currentHand.transform.position.y, toPosition);
        currentHand.transform.position = Vector3.MoveTowards(currentHand.transform.position, targetPosition, handSpeed * Time.deltaTime);
        currentCioccolataHands.PunchAnimator();
        if (Mathf.Abs(currentHand.transform.position.z - targetPosition.z) < 0.1 && !isGrounded) // while is not grounded 
        {
            slapHighlight.SetActive(true);
            slapHighlight.transform.position = new Vector3(currentHand.transform.position.x, slapHighlight.transform.position.y, currentHand.transform.position.z);

            currentHandRb.velocity = new Vector3(currentHandRb.velocity.x, -(walkSpeed * Time.fixedDeltaTime), currentHandRb.velocity.z);
            isGrounded = Physics.Raycast(currentHandBc.bounds.center, Vector3.down, currentHandBc.bounds.extents.y + 0.15f, floormask); // verificar se está no chão ou não
            if(isGrounded)
            {
                currentHandRb.velocity = Vector3.zero;
                currentattack = SlideAttack;
                slapHighlight.SetActive(false);
            }
        }
    }

    private void SlideAttack() // when the hand is on the ground with the raycast
    {
        Debug.Log("Sliding!!");
        currentCioccolataHands.SlideAnimator();
        
        if(Mathf.Abs(currentHand.transform.position.x - player.transform.position.x) > 10f)
        {
           currentHandRb.velocity = Vector3.zero;
            ValuesToReturn(currentHand);
            passHighlight.SetActive(false);
            
        }
        else
        {
            passHighlight.SetActive(true);
            passHighlight.transform.position = new Vector3(transform.position.x, passHighlight.transform.position.y,currentHand.transform.position.z);
            if (currentHand == rightHand)
            {
                currentHandRb.velocity = new Vector3(-(walkSpeed * Time.fixedDeltaTime), rHandrb.velocity.y, rHandrb.velocity.z);
            }
            else
            {
                currentHandRb.velocity = new Vector3((walkSpeed * Time.fixedDeltaTime), rHandrb.velocity.y, rHandrb.velocity.z);
            }
            
        }

    }
    public void HandwasHit(GameObject handhit) // Define which one of the hands was hit by the player
    {
        
        ValuesToReturn(handhit);
    }
    public void ValuesToReturn(GameObject handhit)
    {
        currentattack = null;
        currentHand = handhit == leftHand ? leftHand : rightHand;
        currentOriginalPosition = currentHand == leftHand ? lHandOriginalPos : RHandOriginalPos;
        ReturnTo = BackToStart;
    }
    private void BackToStart() // Hand go to his original position
    {
        currentHand.transform.position = Vector3.MoveTowards(currentHand.transform.position
            , currentOriginalPosition.position, handSpeed * Time.deltaTime);
        currentCioccolataHands.BackToIdleAnimation();
        if(Vector3.Distance(currentOriginalPosition.position , currentHand.transform.position) < 0.1f)
        {
            
            
            ReturnTo = null;
            StartCoroutine(BossAttackAgain());
        }
    }
    private IEnumerator BossAttackAgain() // Wait a few seconds to start again
    {
        currentHand = null;
        currentOriginalPosition = null;
        yield return new WaitForSeconds(4f);
        isBossAttacking = false;
        isGrounded = false;
    }
}
