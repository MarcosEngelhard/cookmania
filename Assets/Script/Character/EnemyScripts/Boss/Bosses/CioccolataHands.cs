﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CioccolataHands : MonoBehaviour
{
    private Player player;
    [SerializeField]private int amountDamage = 20;
    private Cioccolata cioccolata;
    private Rigidbody rb;
    private Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        cioccolata = transform.parent.GetComponent<Cioccolata>();
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cinnamon"))
        {
            rb.velocity = Vector3.zero;
            cioccolata.BossHealthState(amountDamage);
            cioccolata.HandwasHit(this.gameObject);
        }
        if (other.gameObject.CompareTag("Player")) // if the hand hit player, player take some damage
        {
            player.TakeDamage(amountDamage);
        }
    }
    public void PunchAnimator()
    {
        if (animator.GetBool("Punching"))
        {
            return;
        }
        animator.SetBool("Punching", true);
    }
    public void SlideAnimator()
    {
        if (animator.GetBool("Sliding"))
        {
            return;
        }
        animator.SetBool("Sliding", true);
    }
    public void BackToIdleAnimation()
    {
        if (!animator.GetBool("Sliding") && !animator.GetBool("Punching"))
        {
            return;
        }
        animator.SetBool("Punching", false);
        animator.SetBool("Sliding", false);
    }
}
