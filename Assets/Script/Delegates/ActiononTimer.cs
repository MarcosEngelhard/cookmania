﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiononTimer : MonoBehaviour
{
    private float timer;
    public void setTimer(float timer)
    {
        this.timer = timer;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
    }
    public bool IsTimerComplete()
    {
        return timer <= 0f;
    }
}
