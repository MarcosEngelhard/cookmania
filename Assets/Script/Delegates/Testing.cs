﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    [SerializeField] private ActiononTimer actiononTimer;

    public delegate void TestDelegate();
    //public delegate bool TestBoolDelegate(int i);
    private TestDelegate testDelegateFunction;
    //private TestBoolDelegate testBoolDelegateFunction;

    //private Action testAction;
    //private Action<int, float> testIntFloatAction;
    //private Func<bool> testFunc;
    //private Func<int, bool> testingIntBoolFunc; // Func
    // Start is called before the first frame update
    void Start()
    {
        
        //testDelegateFunction = delegate() { Debug.Log("Anonymous method"); };
        //testDelegateFunction = () => { Debug.Log("Lambda expression"); }; //lambda expression
        
        //testDelegateFunction();

        //testIntFloatAction = (int i, float f) => { Debug.Log("Test int float action"); };

        //testFunc = () => false;

        //testingIntBoolFunc = (int i) => { return i < 5; };
        //testBoolDelegateFunction = (int i ) =>  i < 5; ;
        //Debug.Log(testBoolDelegateFunction(1));
    }

    // Update is called once per frame
    void Update()
    {
        //if (!hasTimerElapsed && actiononTimer.IsTimerComplete())
        //{
        //    Debug.Log("Timer complete!");
        //    hasTimerElapsed = true;
        //}
    }
    private void MyTestDelegateFunction()
    {
        Debug.Log("MyTestDelegateFunction");
    }
    private void MySecondTestDelegateFunction()
    {
        Debug.Log("MySecondTestDelegateFunction");
    }

    private bool MyTestBoolDelegateFunction(int i)
    {
        return i < 5;
    }
}
