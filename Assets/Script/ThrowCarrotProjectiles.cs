﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowCarrotProjectiles : MonoBehaviour
{
    private enum State // Which behaviour the throwprojectile is gonna have
    {
        Randomizer, WhereThePlayerIs
    }
    private State state;
    [SerializeField] private Projectile [] CarroProjectielArray;
    [SerializeField]private float maxtimetogenerateCarrot = 6f;
    [SerializeField]private int timebetweenprojectile = 4;
    private Transform target;

    private bool canshoot;
    [SerializeField]private float maxZ = -2.6f;
    [SerializeField]private float minZ = -9.5f;
    private Player player;
    private float[] positionvalueArray;
    private int j = 0;
    private int railPosition;
    [SerializeField]private float DifferenceXToPlayer = 70f;
    

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>();
        positionvalueArray = player.GetRails();
        


        foreach (Transform child in transform) // Get Projectile component from all the childs
        {
            if(child.GetComponent<Projectile>() != null)
            CarroProjectielArray[j] = child.GetComponent<Projectile>();
            j++;
        }
        target = FindObjectOfType<Player>().transform;
        state = (State)Random.Range(0, 2);
        switch (state){
            case State.Randomizer:
                ChanceZPosition();
                break;
            case State.WhereThePlayerIs:
                WhereThePlayerIs();
                break;

        }
        
        
        
        
        canshoot = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (player.IsOnBattle || ButtonsBehaviour.isPaused || GameManager.instance.IsCompleted || GameManager.instance.IsGameOver)
            return;
        transform.position = new Vector3(target.position.x + DifferenceXToPlayer, transform.position.y, transform.position.z);
        if( target.position.x < transform.position.x)
        {
            if (canshoot)
            {
                foreach (Projectile CarrotProjectile in CarroProjectielArray)
                {
                    if (!CarrotProjectile.gameObject.activeInHierarchy)
                    {
                        
                        switch (state)
                        {
                            case State.Randomizer:
                                ChanceZPosition();
                            
                                break;
                            case State.WhereThePlayerIs:
                                WhereThePlayerIs();
                                break;

                        }
                        // Shoot the projectile
                        CarrotProjectile.Rail = railPosition;
                        CarrotProjectile.gameObject.SetActive(true);
                    
                    CarrotProjectile.isFacingLeft = target.position.x < transform.position.x ? true : false;
                    

                        canshoot = false;
                        state = (State)Random.Range(0, 2);
                        StartCoroutine(ShootProjectileAgain());
                        return; // exit from the current update
                    }
                }
            }
            
        }
        
    }
    IEnumerator ShootProjectileAgain() // wait sime time until shoot another projectile again
    {
        yield return new WaitForSeconds(timebetweenprojectile);
        canshoot = true;
    }
    private void ChanceZPosition() // Chance if it's a random case
    {
        railPosition = Random.Range(0,positionvalueArray.Length);
        
        transform.position = new Vector3(transform.position.x, transform.position.y, positionvalueArray[railPosition]);
    }
    private void WhereThePlayerIs() // chance z value in case it's theow to where playing is at case
    {
        var Zvalue = target.transform.position.z;
        transform.position = new Vector3(transform.position.x,transform.position.y,Zvalue);
        railPosition = player.GetPlayerRailPosition();
    }
    
}
