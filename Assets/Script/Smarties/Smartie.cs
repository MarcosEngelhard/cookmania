﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smartie : MonoBehaviour
{
    public enum KindOfSmartie
    {
        Blue, Green, Yellow,
    }
    public KindOfSmartie SmartieType;
    public BoxCollider bc;
    [SerializeField] private LayerMask playerMask;
    // Start is called before the first frame update
    void Start()
    {
        bc = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void PlayerCollision()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position, bc.bounds.extents, Quaternion.identity, playerMask);
        foreach (Collider collider in colliders)
        {
            Debug.Log(collider.name);
            // if collider is not triggered
            if (!collider.isTrigger)
            {
                Player player = collider.gameObject.GetComponent<Player>();
                switch (SmartieType)
                {
                    case KindOfSmartie.Blue:
                        
                        if (!player.CharacterAnimator.GetBool("Attack")) // if brom is not attacking
                        {
                            player.wasOnRythm = GameManager.instance.NormalHit;
                            Debug.Log("Azul");
                        }

                        break;
                    case KindOfSmartie.Green:
                        if (!player.CharacterAnimator.GetBool("Attack"))
                        {
                            player.wasOnRythm = GameManager.instance.GoodHit;
                            Debug.Log("Verde");
                        }


                        break;
                    case KindOfSmartie.Yellow:
                        if (!player.CharacterAnimator.GetBool("Attack"))
                        {
                            player.wasOnRythm = GameManager.instance.PerfectHit;
                            Debug.Log("Amarelo");
                        }


                        break;
                }
            }
        }
    }
}
