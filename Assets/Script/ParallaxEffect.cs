﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    private Transform cameraTransform;
    private Vector3 lastCameraPosition;
    [SerializeField] private Vector2 parallaxEffectMultiplier = new Vector2(.4f,.1f);
    private float textureUnitSizeX;
    private float textureUnitSizeY;
    [SerializeField]private bool allowinYParallax;

    // Start is called before the first frame update
    void Start()
    {
        // Get the Camera Transform
        cameraTransform = Camera.main.transform;
        lastCameraPosition = cameraTransform.position;
        //Figure out text Unite size
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D texture = sprite.texture;
        // the * transform.localscale.x in order to work correctly with scaled sprites
        textureUnitSizeX = (texture.width / sprite.pixelsPerUnit) * transform.localScale.x;
        textureUnitSizeY = (texture.height / sprite.pixelsPerUnit) * transform.localScale.y;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // How much the camera has move since the previous frame
        Vector2 deltamovement = cameraTransform.position - lastCameraPosition;
        transform.position += new Vector3(deltamovement.x * parallaxEffectMultiplier.x, deltamovement.y * parallaxEffectMultiplier.y,0);
        lastCameraPosition = cameraTransform.position;

        if(Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeX)
        {
            // replace in the new position
            float offsetPositionX = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
            transform.position = new Vector3(cameraTransform.position.x + offsetPositionX, transform.position.y,transform.position.z);
        }
        if (allowinYParallax)
        {
            if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeY)
            {
                // replace in the new position
                float offsetPositionY = (cameraTransform.position.y - transform.position.y) % textureUnitSizeY;
                transform.position = new Vector3(cameraTransform.position.x, transform.position.y + offsetPositionY, transform.position.z);
            }
        }
        
    }
}
