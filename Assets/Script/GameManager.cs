﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public delegate void PerfectNoteEffect();
    public PerfectNoteEffect perfectNoteEffect;
    public AudioSource theMusic;

    public GameObject rythmSetup;
    private bool isRythmActive;

    private bool startPlaying;

    public BeatScroll theBS;

    public static GameManager instance; // there can be only one GameManager

    public int CurrentScore;
    public int ScorePerNote = 100;
    public int ScorePerGoodNote = 125;
    public int ScorePerPerfectNote = 150;

    public float CurrentMultiplier;
    [SerializeField] private Text comboText;
    public int multiplierTracker;
    //public int[] MultiplierThresholds;
    public bool enemiesCanAttack;
    public bool aPerfectNote;
    [SerializeField] private float effectTime;

    public GameObject rhytmButton;
    private AudioSource audioSource;
    private Player player;

    private bool isCompleted = false;
    public bool IsCompleted
    {
        get { return isCompleted; }
    }
    private bool isGameOver = false;
    public bool IsGameOver
    {
        get { return isGameOver; }
    }
    [SerializeField] private Image gameOverPanel;

    public enum NoteEffect
    {
        Attack
    }
    public NoteEffect noteEffect;
    private void Awake()
    {
        CurrentMultiplier = 1;
        comboText.text = CurrentMultiplier + "x";
        //rythmSetup = GameObject.FindGameObjectWithTag("Rythm");
        rythmSetup.SetActive(false);
        //isRythmActive = false;
        theMusic.Play();
        audioSource = GetComponent<AudioSource>();
        player = FindObjectOfType<Player>();
    }

    // Start is called before the first frame update
    void Start()
    {
        perfectNoteEffect = null;
        instance = this;
        
        aPerfectNote = false;
        enemiesCanAttack = false;
        gameOverPanel.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //public void StartRhythm()
    //{
    //    if (!startPlaying)
    //    {
    //        if (Input.anyKeyDown)
    //        {
    //            startPlaying = true;
    //            theBS.hasStarted = true;


    //        }
    //    }
    //}

    public void NoteHit(NoteObject noteObject) // if hit on Time
    {
        
        DebugEffect();
        rhytmButton.GetComponent<ParticleSystem>().Play();
    }
    public void NormalHit(/*NoteObject noteObject*/) // Normal hit
    {

        //CurrentScore += ScorePerNote * CurrentMultiplier;
        CurrentMultiplier += 1;
        comboText.text = CurrentMultiplier + "x";
        //aPerfectNote = true;


        // NoteHit(noteObject);
    }
    public void GoodHit(/*NoteObject noteObject*/) // Good hit
    {

        //CurrentScore += ScorePerGoodNote * CurrentMultiplier;
        CurrentMultiplier += 1.5f;
        comboText.text = CurrentMultiplier + "x";
        //aPerfectNote = true;


        //NoteHit(noteObject);
    }

    public void PerfectHit(/*NoteObject noteObject*/) // Perfect hit
    {

        //CurrentScore += ScorePerPerfectNote * CurrentMultiplier;
        //CurrentMultiplier += 2f;
        CurrentMultiplier = CurrentMultiplier + 2f;
        Debug.Log("CurrentMultiplier " + CurrentMultiplier);
        comboText.text = CurrentMultiplier + "x";
        //aPerfectNote = true;


        //NoteHit(noteObject);
    }

    public void NoteMissed() // if the note is missed
    {

        // CurrentMultiplier = 1;
        //multiplierTracker = 0;
        
        GameObject[] EnemyArray = GameObject.FindGameObjectsWithTag("Enemy");
        foreach(GameObject enemy in EnemyArray)
        {
            Enemy enemyscript = enemy.GetComponent<Enemy>();
            if(enemyscript != null)
            {
                enemyscript.AttackOnBeat();
            }
            
        }
        
      
    }
   
    IEnumerator StopAttackEffect()
    {
        yield return new WaitForSeconds(effectTime);
        //Player.playerinstance.SetDefaultAttack();
        if (perfectNoteEffect != null)
        {
            perfectNoteEffect = null;
        }

        //Playermovement.pmovementinstance.ReturnToDefaultMultiplier();
    }
    public void DebugEffect() // 
    {
        aPerfectNote = true;
        switch (noteEffect)
        {
            
            case NoteEffect.Attack:
                StopCoroutine(StopAttackEffect());
                StartCoroutine(StopAttackEffect());
                break;
        }
        
    }
    public void TurnRythmSetupOnOff()
    {
        theMusic.Stop();
        theMusic.Play();
        if (isRythmActive)
        {
            rythmSetup.SetActive(false);
            isRythmActive = !isRythmActive;
        }
        else
        {
            rythmSetup.SetActive(true);
            isRythmActive = !isRythmActive;
        }
    }
    public void SetGameCompletes()
    {
        isCompleted = true;

    }
    public void produceDeathSound(AudioClip audioClip)
    {
        audioSource.PlayOneShot(audioClip);
    }

    public void GameOver()
    {
        isGameOver = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        //Time.timeScale = 0;
        gameOverPanel.gameObject.SetActive(true);
        player.DisableInputKeys();
    }
    public void IncreaseHighScoreToPlayer(Player player)
    {
        if (CurrentMultiplier != 1)
        {
            player.HighScore = player.HighScore != 0 ? player.HighScore + ((int)(player.highscoreNormal * CurrentMultiplier))
                :  ((int)(player.highscoreNormal * CurrentMultiplier));
            CurrentMultiplier = 1f;
            comboText.text = CurrentMultiplier + "x";
        }
    }


}
