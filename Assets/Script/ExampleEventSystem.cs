﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleEventSystem : MonoBehaviour
{
    public event EventHandler OnSpacePressed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DisableScripts()
    {
        OnSpacePressed?.Invoke(this, EventArgs.Empty);
    }
}
