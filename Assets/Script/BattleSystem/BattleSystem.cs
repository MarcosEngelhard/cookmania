﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleSystem : MonoBehaviour
{
    private enum State
    {
        Idle,CountDownTimer, Active, BattleOver,
    }
    
    [SerializeField] private ColliderBattleSystemTrigger colliderTrigger;
    [SerializeField] private Wave[] waveArray;
    private Player player;
    [SerializeField] private float startingCountdownTime = 4f;
    private float currentCountdownTime;
    [SerializeField] private Text countdownText;
    [SerializeField] private Transform playerSpotToBattle; // Spot where the player is gonna move automatic
    private AudioSource audioSource;
    [SerializeField]private AudioClip CountdownHigh;
    [SerializeField]private AudioClip CountdownLow;
    [SerializeField] private AudioClip Sucesso;
    [SerializeField] private Canvas canvas;

    

    private State state;
    private void Awake()
    {
        state = State.Idle;
        player = FindObjectOfType<Player>();
        currentCountdownTime = startingCountdownTime;
        countdownText.gameObject.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        if (canvas.renderMode != RenderMode.ScreenSpaceCamera)
            canvas.renderMode = RenderMode.ScreenSpaceCamera;

        
    }

    // Start is called before the first frame update
    void Start()
    {
        colliderTrigger.OnPlayerTriggerEvent += ColliderTrigger_OnPlayerEnterTrigger;
    }

    private void ColliderTrigger_OnPlayerEnterTrigger(object sender, EventArgs e)
    {
        if(state == State.Idle)
        {
            StartBattle();
            colliderTrigger.OnPlayerTriggerEvent -= ColliderTrigger_OnPlayerEnterTrigger; // make sure it's trigger only once
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!ButtonsBehaviour.isPaused)
        {
            switch (state)
            {
                case State.CountDownTimer:
                    // The coutdown
                    currentCountdownTime -= 1 * Time.deltaTime;
                    countdownText.text = currentCountdownTime.ToString("0");
                    if (!audioSource.isPlaying)
                    {
                        audioSource.PlayOneShot(CountdownLow);
                    }
                    if (currentCountdownTime <= 0)
                    {
                        // Battle will start
                        countdownText.text = "Fight";
                        audioSource.PlayOneShot(CountdownHigh);
                        StartCoroutine(BattleActivated());
                    }
                    break;
                case State.Active:
                    foreach (Wave wave in waveArray)
                    {
                        //wave.Update();
                        if (!wave.isInstantiating)
                        {
                            StartCoroutine(wave.SpawningEnemies());
                        }

                    }
                    TestBattleOver();
                    break;

            }
        }
        
        
    }
    private void StartBattle() // Start the Battle
    {
        Debug.Log("Start battle!");
        state = State.CountDownTimer;
        countdownText.gameObject.SetActive(true);
        player.IsOnBattle = true;
        player.GetSpotTarget(playerSpotToBattle);
        Camerabehaviour.camerainstance.cameraType = Camerabehaviour.CameraType.LockedCamera;
        


    }
    private void TestBattleOver() // Check if battle is over
    {
        if(state == State.Active)
        {
            if (IsEverySingleWaveOver())
            {
                // Battle is over!!
                state = State.BattleOver;
                player.IsOnBattle = false;
                GameManager.instance.TurnRythmSetupOnOff();
                Camerabehaviour.camerainstance.cameraType = Camerabehaviour.CameraType.FreeCamera;
                this.gameObject.SetActive(false);
                colliderTrigger.gameObject.SetActive(false);
                
            }
        }
    }
    private bool IsEverySingleWaveOver() // Check if all waves are over
    {
        foreach(Wave wave in waveArray)
        {
            if (wave.IsWaveOver())
            {
                audioSource.PlayOneShot(Sucesso);
                canvas.renderMode = RenderMode.ScreenSpaceCamera;
                // Wave is over
            }
            else
            {
                // Wave not over
                return false;
            }
            
        }
        return true;
    }
    public IEnumerator BattleActivated()
    {
        state = State.Active;
        yield return new WaitForSeconds(1);
        countdownText.gameObject.SetActive(false);
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        GameManager.instance.TurnRythmSetupOnOff();
        
        

    }

    // Represents a single Enemy Spawn Wave
    [System.Serializable] // in order to represent in the inspector
    private class Wave
    {
        [SerializeField] private EnemySpawn[] enemySpawnPointArray;
        [SerializeField] private float timer;
        [SerializeField] private float timebetweenenemies = 2.5f;
        [SerializeField] public bool isInstantiating = false;
        private bool hasSpawned = false;

        //public void Update()
        //{

        //    if(timer > 0) // can include the zero in case you want to start as soon the battle starts
        //    {
        //        timer -= Time.deltaTime;
        //        if (timer < 0)
        //        {
        //            SpawnEnemies();
                    
        //        }
                
        //    }
            
        //}
        
        //private void SpawnEnemies()
        //{
           
        //}
        
        public bool IsWaveOver()
        {
            if(/*timer < 0*/ hasSpawned) // check if all enemies has spawned
            {
                // Wave spawned
                foreach(EnemySpawn enemySpawn in enemySpawnPointArray)
                {
                    if (enemySpawn.IsAlive())
                    {
                        return false;
                    }
                    
                    
                }
                return true;
            }
            else
            {
                //Enemies haven't spawn  yet
                return false;
            }
        }
        
        public IEnumerator SpawningEnemies()
        {
            isInstantiating = true;
            foreach (EnemySpawn enemySpawn in enemySpawnPointArray) // Instantiating the enemies
            {
                
                enemySpawn.InstantiateEnemy();
                yield return new WaitForSeconds(timebetweenenemies);
            }
            hasSpawned = true;
        }
        
    }
}
