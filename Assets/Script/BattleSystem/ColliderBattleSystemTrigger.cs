﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderBattleSystemTrigger : MonoBehaviour
{
    public event EventHandler OnPlayerTriggerEvent;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Player player = other.GetComponent<Player>();
        if(player != null)
        {
            // Player inside trigger area
            
            
            
            OnPlayerTriggerEvent?.Invoke(this, EventArgs.Empty);
        }
    }
}
