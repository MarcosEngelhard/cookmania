﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    //[SerializeField] private GameObject [] Enemies;  // assigning the enemies
    private GameObject Enemy;
    // Start is called before the first frame update
    void Start() // Get the enemy who is child
    {
        Enemy = transform.GetChild(0).gameObject;
        Enemy.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void InstantiateEnemy() // "Instantiate" the enemy
    {
        Enemy.SetActive(true);
        //Enemy = Instantiate(Enemies[0], transform.position, transform.rotation);
    }
    
    public bool IsAlive() // check if is alive
    {
        if(Enemy != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
