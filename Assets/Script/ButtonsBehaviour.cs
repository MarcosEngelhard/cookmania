﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class ButtonsBehaviour : MonoBehaviour
{
    public static bool isPaused;
    [SerializeField] private GameObject PauseMenu;
    [SerializeField]private float unpauseTime = 4f;
    private float currentTime;
    [SerializeField]private Text timeText;
    [SerializeField] private AudioSource audioSource;
    private enum PauseState
    {
        ResumePausing,Counting
    }
    private PauseState pauseState;
    private void Awake()
    {
        if(audioSource != null)
        {
            audioSource.Pause();
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        isPaused = false;
        if(PauseMenu != null)
        {
            PauseMenu.gameObject.SetActive(false);
        }
        pauseState = PauseState.ResumePausing;
        currentTime = unpauseTime;
        if(timeText != null)
        {
            timeText.gameObject.SetActive(false);
        }
        if(SceneManager.GetActiveScene().buildIndex >= 1)
        {
            
            Cursor.visible = true;
            Time.timeScale = 0f;
            CountingToResumeGame();
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu != null) // Se houver PauseMenu
        {
            switch (pauseState)
            {
                case PauseState.ResumePausing:
                    if (Input.GetKeyDown(KeyCode.Escape) /*|| CrossPlatformInputManager.GetButtonDown("Settings")*/) // the commented one only works in mobile
                    {
                        if (isPaused)
                        {
                            CountingToResumeGame();
                        }
                        else
                        {
                            PauseGame();
                        }
                    }
                    break;
                case PauseState.Counting:
                    currentTime -= Time.unscaledDeltaTime; // Decrease the time independently the timeScale
                    timeText.text ="Resuming in: " + currentTime.ToString("0");
                    if (currentTime <= 0)
                    {
                        timeText.text = "Resume!";
                        currentTime = 0;
                        currentTime += unpauseTime;
                        ResumeGame();
                    }
                    //else
                    //{
                        
                    //}
                    break;
            }
            
        }
    }
    public void ChooseCharacter()
    {
        SceneManager.LoadScene(1);
    }
    public void PlayNewGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(1);
    }
    public void Options()
    {
        Debug.Log("Ir para as opções!!");
    }
    public void ExitGame() // Quit Game
    {
        Application.Quit();
        Debug.Log("Sair!!");
    }
    public void CountingToResumeGame() // Activate in hierarqui the pause menu and put the scale to normal
    {
        PauseMenu.SetActive(false);
        timeText.gameObject.SetActive(true);
        pauseState = PauseState.Counting;

        
    }
    private void ResumeGame()
    {
        audioSource.UnPause();
        Time.timeScale = 1f;
        isPaused = false;
        timeText.gameObject.SetActive(false);
        Cursor.visible = false;
        pauseState = PauseState.ResumePausing;
    }
    public void PauseGame() // DeActivate in hierarqui the pause menu and freeze the scale
    {
        audioSource.Pause();
        PauseMenu.SetActive(true);
        isPaused = true;
        Cursor.visible = true;
        Time.timeScale = 0f;
        
    }
    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void GoToMainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }
}
