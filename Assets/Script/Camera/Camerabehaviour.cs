﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camerabehaviour : MonoBehaviour
{
    public enum CameraType
    {
        FreeCamera, LockedCamera,
    }
    private GameObject playerTransform;
    [SerializeField]private Vector3 followOffSet;
    [SerializeField] private Vector3 lockedOffSet;
    [SerializeField] private float smoothspeed = 0.125f; // how fast we want the camera moves
    public CameraType cameraType;
    public static Camerabehaviour camerainstance;
    private Vector3 freeCameraFollowOffSet;
    private float ZValueConstant;
    
    

    // Start is called before the first frame update
    void Start()
    {
        playerTransform = FindObjectOfType<Player>().gameObject;
        cameraType = CameraType.FreeCamera;
        camerainstance = this;
        freeCameraFollowOffSet = followOffSet;
        // Set Cursor to not be visible
        Cursor.visible = false;
        ZValueConstant = playerTransform.transform.position.z + followOffSet.z;
        
        
    }
    

    // Update is called once per frame
    void FixedUpdate()
    {
        switch (cameraType)
        {
            case CameraType.FreeCamera:
                if (playerTransform != null)
                {
                    if(followOffSet != freeCameraFollowOffSet)
                    {
                        followOffSet = freeCameraFollowOffSet;
                    }
                    Vector3 desiredPosition = playerTransform.transform.position + followOffSet;
                    desiredPosition.z = ZValueConstant;
                    Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothspeed);
                    transform.position = smoothedPosition;
                }
                break;
            case CameraType.LockedCamera:
                if(playerTransform != null)
                {
                    if(followOffSet != lockedOffSet)
                    {
                        followOffSet = lockedOffSet;
                    }
                    
                    Vector3 desiredPosition = playerTransform.transform.position + followOffSet;
                    Vector3 smoothedPosition = Vector3.Lerp(transform.position,
                        desiredPosition, smoothspeed);
                    transform.position = smoothedPosition;
                }
                break;
        }
        
        
    }
    
    
}
