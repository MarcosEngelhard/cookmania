﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShaking : MonoBehaviour
{
    public static CameraShaking Instance { get; set; }
    private CinemachineVirtualCamera cinemachineVirtualCamera;
    private CinemachineTransposer cinemachineTransposer;
    
    // Start with the time
    private float shakeTimer = 0;
    private float shakeTimerTotal = 5f;
    private float startingIntensity;
    private float intensity = 7;
    private float time = 0.2f;
    private bool isOntheMiddle = false;
     
    void Awake()
    {
        Instance = this;
        cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
        cinemachineTransposer = cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTransposer>();
        //CinemachineTransposer.m_FollowOffset.z
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(shakeTimer > 0)
        {

            shakeTimer -= Time.deltaTime;
            if(shakeTimer <= 0f)
            {
                //Time over!
                CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin =
                cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

                cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0;
                //Mathf.Lerp(startingIntensity, 0f, 1 - (shakeTimer / shakeTimerTotal)); // Linear Interpolate
            }
                
            
            
        }
    }
    public void ShakeTheCamera(/*float intensity, float time*/)
    { 
        // The nose of the camera
        CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin =
            cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;
        startingIntensity = intensity;
        shakeTimerTotal = 5f;
        shakeTimer = 0.2f;
    }
    public void PutPlayerOnMiddle() // Make that the player is in the middle of the camera by making the offset of x axis to 0
    {
        if (!isOntheMiddle)
        {
            cinemachineTransposer.m_FollowOffset.x -= Time.deltaTime;
        if (cinemachineTransposer.m_FollowOffset.x <= 0)
            {
                cinemachineTransposer.m_FollowOffset.x = 0;
                Debug.Log("No meio!");
                isOntheMiddle = true;
                GameManager.instance.SetGameCompletes(); // make the game completed
                return;
            }
            
        }
        
    }

}
